;;; ~/.config/doom/config.el -*- lexical-binding: t; -*-

(setq doom-font (font-spec :family "Hermit" :size 18)
      doom-variable-pitch-font (font-spec :family "Noto Sans" :size 14))

(setq company-idle-delay 0.2
      company-minimum-prefix-length 2)
      
(setq org-lone-done 'time)

(load-theme 'doom-dracula t)
