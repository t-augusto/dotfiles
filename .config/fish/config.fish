# Make UTF-8
set -g -x LC_ALL en_US.utf8
set -g -x LC_LANG en_US.utf8
set -g -x LANG en_US.utf8

set -g -x PATH $HOME/bin /usr/bin /bin /usr/sbin /sbin /usr/local/bin
set -g -x BROWSER firefox
set -g -x EDITOR vim
set -g -x TERMINAL urxvt
set -g -x XDG_CONFIG_HOME $HOME/.config
