set nocompatible
"Remove swap file
set noswapfile

"Syntax Highlighting
set background=dark
colorscheme term
syntax on
filetype plugin on

" lazy file name tab completion
set wildmode=longest,list,full
set wildmenu
set wildignorecase

" ignore files vim doesnt use
set wildignore+=.git,.hg,.svn
set wildignore+=*.aux,*.out,*.toc
set wildignore+=*.o,*.obj,*.exe,*.dll,*.manifest,*.rbc,*.class
set wildignore+=*.ai,*.bmp,*.gif,*.ico,*.jpg,*.jpeg,*.png,*.psd,*.webp
set wildignore+=*.avi,*.divx,*.mp4,*.webm,*.mov,*.m2ts,*.mkv,*.vob,*.mpg,*.mpeg
set wildignore+=*.mp3,*.oga,*.ogg,*.wav,*.flac
set wildignore+=*.eot,*.otf,*.ttf,*.woff
set wildignore+=*.doc,*.pdf,*.cbr,*.cbz
set wildignore+=*.zip,*.tar.gz,*.tar.bz2,*.rar,*.tar.xz,*.kgb
set wildignore+=*.swp,.lock,.DS_Store,._*

"Ignore cases
set ignorecase
set smartcase
set infercase

set number

"Spaces for tabs
set expandtab
" 1 Tab is 4 spaces
set shiftwidth=4
set tabstop=4
set nowrap

"search
set hlsearch

" Match brackets/curly/parenthesis stuff
set showmatch
set mouse=a
