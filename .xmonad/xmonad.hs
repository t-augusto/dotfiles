import XMonad

import XMonad.Config.Desktop
import XMonad.Actions.SinkAll
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.SetWMName

import XMonad.Layout.BinarySpacePartition as BSP
import XMonad.Layout.Tabbed
import XMonad.Layout.LayoutModifier (ModifiedLayout)
import XMonad.Layout.MultiToggle
import XMonad.Layout.Spacing
import XMonad.Layout.WindowNavigation

import XMonad.Util.EZConfig(additionalKeys)
import XMonad.Util.NamedScratchpad
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.SpawnOnce

import Graphics.X11.ExtraTypes.XF86
import System.IO
import System.Taffybar.Hooks.PagerHints (pagerHints)

myStartupHook = do
    spawnOnce "sh ~/.screenlayout/novo_dual.sh"
    spawnOnce "compton -b --config ~/.config/compton.conf"
    spawnOnce "dunst"

myLayoutHook = id
    $ avoidStruts
    $ layoutHook defaultConfig

myLayout = nav BSP.emptyBSP ||| simpleTabbed 
    where nav :: LayoutClass l a => l a -> ModifiedLayout WindowNavigation l a
          nav = configurableNavigation noNavigateBorders

myScratchpads = [
        NS "volume" "pavucontrol"
        (className =? "Pavucontrol")
        defaultFloating
                ]

main = do
    xmonad $ ewmh $ pagerHints $ docks desktopConfig
        { manageHook = manageDocks
                   <+> namedScratchpadManageHook myScratchpads
                   <+> manageHook defaultConfig
        , layoutHook = avoidStruts $ myLayout
        , handleEventHook = fullscreenEventHook
        , startupHook = setWMName "LG3D" 
        , modMask = mod4Mask
        , terminal = "st"
        , borderWidth = 3
        , focusedBorderColor = "#6c71c4"
        } `additionalKeys` myKeys


myKeys = [
            ((controlMask, xK_Print), spawn "sleep 0.2; scrot -s")
          , ((mod4Mask,                            xK_p     ), spawn "exec dmenu_run -b -l 15 -fn FantasqueSansMono-12")
          , ((0, xK_Print), spawn "scrot")
          , ((controlMask .|. mod1Mask,            xK_l     ), spawn "i3lock-fancy")
          , ((controlMask .|. mod1Mask,            xK_f     ), sinkAll)
          , ((mod4Mask,                            xK_l     ), sendMessage $ Go R)
          , ((mod4Mask,                            xK_h     ), sendMessage $ Go L)
          , ((mod4Mask,                            xK_j     ), sendMessage $ Go D)
          , ((mod4Mask,                            xK_k     ), sendMessage $ Go U)
          , ((mod4Mask .|. controlMask,            xK_h     ), sendMessage $ XMonad.Layout.WindowNavigation.Swap L)
          , ((mod4Mask .|. controlMask,            xK_j     ), sendMessage $ XMonad.Layout.WindowNavigation.Swap D)
          , ((mod4Mask .|. controlMask,            xK_k     ), sendMessage $ XMonad.Layout.WindowNavigation.Swap U)
          , ((mod4Mask .|. controlMask,            xK_l     ), sendMessage $ XMonad.Layout.WindowNavigation.Swap R)
          , ((mod4Mask .|. mod1Mask,               xK_l     ), sendMessage $ BSP.ExpandTowards R)
          , ((mod4Mask .|. mod1Mask,               xK_h     ), sendMessage $ BSP.ExpandTowards L)
          , ((mod4Mask .|. mod1Mask,               xK_j     ), sendMessage $ BSP.ExpandTowards D)
          , ((mod4Mask .|. mod1Mask,               xK_k     ), sendMessage $ BSP.ExpandTowards U)
          , ((mod4Mask .|. mod1Mask .|. controlMask , xK_l  ), sendMessage $ BSP.ShrinkFrom R)
          , ((mod4Mask .|. mod1Mask .|. controlMask , xK_h  ), sendMessage $ BSP.ShrinkFrom L)
          , ((mod4Mask .|. mod1Mask .|. controlMask , xK_j  ), sendMessage $ BSP.ShrinkFrom D)
          , ((mod4Mask .|. mod1Mask .|. controlMask , xK_k  ), sendMessage $ BSP.ShrinkFrom U)
          , ((mod4Mask,                            xK_r     ), sendMessage BSP.Rotate)
          , ((mod4Mask,                            xK_s     ), sendMessage BSP.Swap)
          , ((mod4Mask,                            xK_n     ), sendMessage FocusParent)
          , ((0, xF86XK_AudioLowerVolume   ), spawn "pactl set-sink-volume 1 -- -1.5%")
          , ((0, xF86XK_AudioRaiseVolume   ), spawn "pactl set-sink-volume 1 +1.5%")
          , ((0, xF86XK_AudioMute          ), spawn "pactl set-sink-mute 1 toggle")
          , ((mod1Mask,                            xK_m     ), namedScratchpadAction myScratchpads "volume")
         ]


